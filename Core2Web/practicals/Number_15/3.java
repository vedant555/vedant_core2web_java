import java.io.*;

class Cdemo{
	public static void main(String[] args)throws IOException{
		BufferedReader br=new BufferedReader(new InputStreamReader(System.in));
		int num=Integer.parseInt(br.readLine());
		int temp=1;
		int count=0;
		while(temp<=num){
			if(num%temp==0){
				count++;
			}temp++;
		}if(count>=3){
			System.out.println(num+" is a composite number");
		}else
			System.out.println(num+" is not a composite number");
	}
}

