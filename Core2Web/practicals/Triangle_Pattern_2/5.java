import java.io.*;

class Edemo{
	public static void main(String[] args)throws IOException{
		BufferedReader br=new BufferedReader(new InputStreamReader(System.in));
		int row=Integer.parseInt(br.readLine());	
		int num =65+row;
		for(int i=1; i<=row; i++){	
			   for(int j=row; j<=row+i-1; j++){
       	        	System.out.print((char)num++ +" ");			
			}
		                System.out.println();
		}
	}
}

