import java.io.*;

class Bdemo{
	public static void main(String[] args)throws IOException{
		BufferedReader br=new BufferedReader(new InputStreamReader(System.in));
		int rows=Integer.parseInt(br.readLine());
		for(int i=1; i<=rows; i++){
			int num=97;
			for(int j=rows; j<=rows+i-1; j++){
				if(i%2==1){
					System.out.print((char)(num)+" ");
					num++;
				}else{
					System.out.print("$"+" ");
				}
			}	System.out.println();
			
		}
	}
}
