class Concept{
	public static void main(String[] args){
		int x = 55;
		int y = 75;
		System.out.println(x+y);                   //arithmetic
		System.out.println(++x + ++y);             //unary
		System.out.println(x>=y);                  //relational
		System.out.println(x*=y);                  //assignment
		System.out.println(x | y);                 //bitwise
	}
}

