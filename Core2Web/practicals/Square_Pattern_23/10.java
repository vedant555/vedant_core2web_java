import java.io.*;

class Jdemo{
	public static void main(String[] args)throws IOException{
		BufferedReader br=new BufferedReader(new InputStreamReader(System.in));
		int rows=Integer.parseInt(br.readLine());
		int num=rows*rows;
		for(int i=1; i<=rows; i++){
			for(int j=1; j<=rows; j++){
				if(i==j)
					System.out.print("$"+" ");					
				else
					System.out.print(num*j+" ");				
				        num--;			
			}
					System.out.println();
		}
	}
}
				
