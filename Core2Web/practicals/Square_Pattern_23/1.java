import java.io.*;

class Ademo{
	public static void main(String[] args)throws IOException{
		BufferedReader br=new BufferedReader(new InputStreamReader(System.in));
		int rows=Integer.parseInt(br.readLine());
		int num=rows;
		for(int i=1; i<=rows; i++){
			for(int j=1; j<=rows; j++){
				if(j==1){
					int num2=num+64;
					System.out.print((char)num2+" ");
				}else{
					int num1=num+96;
					System.out.print((char)num1+" ");
				}
			num++;
			}
			System.out.println();
		}
	}
}
