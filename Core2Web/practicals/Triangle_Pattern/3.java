import java.io.*;

class Gdemo{
	public static void main(String[] args)throws IOException{
		BufferedReader br=new BufferedReader(new InputStreamReader(System.in));
		int row=Integer.parseInt(br.readLine());
		for(int i=1; i<=row; i++){
			int num=i+64;
			for(int j=row; j<=row+i-1; j++){
				System.out.print((char)num +" ");
				num++;
			}
			System.out.println();
		}
	}
}
