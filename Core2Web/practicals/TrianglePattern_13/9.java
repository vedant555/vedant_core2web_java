import java.io.*;

class Jdemo{
	public static void main(String[] args)throws IOException{
		BufferedReader br=new BufferedReader(new InputStreamReader(System.in));
		int row=Integer.parseInt(br.readLine());
		int num=row+1;
		int num2=row*num-1;
		for(int i=1; i<=row; i++){		
			for(int j=1; j<=row-i+1; j++){
				System.out.print(num2 +" ");
				num2-=2;
			}
			System.out.println();			
		}
	}
}
