import java.io.*;

class Bdemo{
	public static void main(String[] args)throws IOException{
		BufferedReader br=new BufferedReader(new InputStreamReader(System.in));
		int rows=Integer.parseInt(br.readLine());
		int num=0;
		for(int i=1; i<=rows; i++){
			for(int j=1; j<=rows-i+1; j++){
                                  num+=2;
				System.out.print(num +" ");
			}
			System.out.println();
		}
	}
}
