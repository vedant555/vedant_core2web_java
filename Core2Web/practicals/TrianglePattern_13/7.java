import java.io.*;

class Hdemo{
	public static void main(String[] args)throws IOException{
		BufferedReader br=new BufferedReader(new InputStreamReader(System.in));
		int rows=Integer.parseInt(br.readLine());	
		for(int i=1; i<=rows; i++){
			for(int j=1; j<=rows-i+1; j++){
				int num=rows-i-j+2;
				int num2=rows+96-i-j+2;
				if(j%2==1){
					System.out.print(num +" ");
				}else{
					System.out.print((char)num2 +" ");
					num2--;
				}
			}
			System.out.println();
		}
	}
}
