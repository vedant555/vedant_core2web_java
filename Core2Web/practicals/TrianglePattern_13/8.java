import java.io.*;

class Idemo{
	public static void main(String[] args)throws IOException{
		BufferedReader br=new BufferedReader(new InputStreamReader(System.in));
		int rows=Integer.parseInt(br.readLine());
		for(int i=1; i<=rows; i++){      			                          
      			for(int j=1; j<=rows-i+1; j++){
		          int num=rows-i-j+2;
			  int ch=64+rows-i-j+2;
				if(i%2==1){
					System.out.print(num +" ");
				}else{
					System.out.print((char)ch +" ");
				}
			}
			System.out.println();
		}
	}
}
