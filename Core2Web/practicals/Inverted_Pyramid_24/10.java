import java.io.*;

class Jdemo{
	public static void main(String[] args)throws IOException{
		BufferedReader br=new BufferedReader(new InputStreamReader(System.in));
		System.out.print("Enter no of rows: ");
		int row= Integer.parseInt(br.readLine());
		int num=row;
		for(int i=1; i<=row; i++){
			for(int sp=1; sp<i; sp++){
				System.out.print("\t");
			}
			for(int j=1; j<=(row-i)*2+1; j++){
				if(j<row-i+1)
					System.out.print(num-- +"\t");
				else
					System.out.print(num++ +"\t");
			}num-=2;
					System.out.println();
		}
	}
}
			

