import java.io.*;

class Ddemo{
	public static void main(String[] args)throws IOException{
		BufferedReader br=new BufferedReader(new InputStreamReader(System.in));
		System.out.print("Enter no of rows: ");
		int row=Integer.parseInt(br.readLine());
		for(int i=1; i<=row; i++){
			for(int sp=1; sp<i; sp++){
				System.out.print("\t");
			}int num=1;
			for(int j=1; j<=(row-i)*2+1; j++){
				System.out.print(num++ +"\t");
			}
			System.out.println();
		}
	}
}
