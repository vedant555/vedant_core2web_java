import java.io.*;

class Idemo{
	public static void main(String[] args)throws IOException{
		BufferedReader br=new BufferedReader(new InputStreamReader(System.in));
		System.out.print("Enter no of rows: ");
		int row=Integer.parseInt(br.readLine());
		for(int i=1; i<=row; i++){
			for(int sp=1; sp<i; sp++){
				System.out.print("\t");
			}
			int num1=1;
			int num2=0;
			for(int j=1; j<=(row-i)*2+1; j++){
				if(j%2==1)
					System.out.print(num1+"\t");
				else
					System.out.print(num2+"\t");
			}
					System.out.println();
		}
	}
}

