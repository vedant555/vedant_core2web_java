import java.util.*;

class Fdemo{
	public static void main(String[] args){
		Scanner sc=new Scanner(System.in);
		System.out.println("Enter the size:" );
		int size=sc.nextInt();
		int arr[]=new int[size];
		System.out.println("Enter elements:" );
		for(int i=0; i<arr.length; i++){
			arr[i]=sc.nextInt();
		}
		int prod=1;
		for(int i=0; i<arr.length; i++){
			if(i%2==1){
				prod*=arr[i];
			}
		}	System.out.println("product of odd indexed elements:"+prod);
		
	}
}
