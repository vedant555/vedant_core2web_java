
import java.util.*;

class Edemo{
	public static void main(String[] args){
		Scanner sc=new Scanner(System.in);
		int size=sc.nextInt();
		int arr[]=new int [size];
		for(int i=0; i<arr.length; i++){
			arr[i]=sc.nextInt();
		}
		int temp=0; 
		System.out.print("reversed array: ");
		for(int i=0; i<arr.length/2; i++){
			temp=arr[i];
                        arr[i]=arr[size-i-1];
			arr[size-i-1]=temp;
			
			System.out.print(arr[i]+" "+temp+" ");
		}
	}
}
			
