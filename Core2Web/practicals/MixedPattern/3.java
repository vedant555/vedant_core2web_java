import java.io.*;

class Cdemo{
	public static void main(String[] args)throws IOException{
		BufferedReader br=new BufferedReader(new InputStreamReader(System.in));
		int rows=Integer.parseInt(br.readLine());
		for(int i=1; i<=rows; i++){
			int ch=64+rows;
			int num=1;
			for(int j=1; j<=rows; j++){
				if(i%2==1){
					System.out.print((char)ch +" ");
					ch--;
				}else{
					System.out.print(num++ +" ");
				}
			}
					System.out.println();
		}
	}
}
			
