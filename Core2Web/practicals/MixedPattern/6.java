import java.io.*;

class Fdemo{
	public static void main(String[] args)throws IOException{
		BufferedReader br=new BufferedReader(new InputStreamReader(System.in));
		int rows=Integer.parseInt(br.readLine());
		for(int i=1; i<=rows; i++){
			int num=rows;
			int ch=rows+96;
			for(int j=rows; j<=rows+i-1; j++){
				if(i%2==1){
				System.out.print((char)ch+" ");
				ch--;
				}else{
					System.out.print(num-- +" ");
				}
			}
			System.out.println();
		}
	}
}
				
