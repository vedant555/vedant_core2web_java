import java.io.*;

class Edemo{
	public static void main(String[] args)throws IOException{
		BufferedReader br=new BufferedReader(new InputStreamReader(System.in));
		int rows=Integer.parseInt(br.readLine());
		for(int i=1; i<=rows; i++){
			int multi=1;
			for(int j=rows; j<=rows+i-1; j++){
				System.out.print(i*multi+" ");
				multi++;
			}
			System.out.println();
		}
	}
}
