
import java.util.*;

class Ademo{
	public static void main(String[] args){
		Scanner sc=new Scanner(System.in);
		int size=sc.nextInt();
		int arr[]=new int [size];
		for(int i=0; i<arr.length; i++){
			arr[i]=sc.nextInt();
		}
		int max=0;
		int cnt=0;
		for(int i=0; i<arr.length; i++){
			if(arr[i]>max){
				max=arr[i];
			     	cnt=0;
			}
			else{
				cnt++;
			}
		}
		if(cnt==0)
		System.out.println("Array is ascending order");
		else
		System.out.println("Array is not ascending order");
	}
}
