import java.util.*;

class Cdemo{
	public static void main(String[] args){
		Scanner sc=new Scanner(System.in);
		int size=sc.nextInt();
		int arr[]=new int [size];
		int temp1=0;
		for(int i=0; i<arr.length; i++){
			arr[i]=sc.nextInt();				
		 temp1=arr[i];
		}		
		int temp2=0;
		for(int i=0; i<arr.length/2; i++){
			temp2=arr[i];
			arr[i]=arr[size-i-1];
			temp2=arr[i];
		}
		
		if(temp1==temp2)
		System.out.println("The given array is palindrome array");
		else
		System.out.println("The given array is not palindrome array");
	}
}
