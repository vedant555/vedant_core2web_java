import java.io.*;

class Ddemo{
	public static void main(String[] args)throws IOException{
		BufferedReader br=new BufferedReader (new InputStreamReader(System.in));
		int rows=Integer.parseInt (br.readLine());
		int col=0;
		int num=rows;
		for(int i=1; i<rows*2; i++){
			if(i<=rows)
				col=i;
			else
				col=rows*2-i;
					for(int j=1; j<=col; j++){
						System.out.print(num+"\t");
					}
					if(i<rows)
						num--;
					else
						num++;
					System.out.println();
		}
	}
}
			
