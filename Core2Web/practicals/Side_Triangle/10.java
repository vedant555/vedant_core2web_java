import java.io.*;

class Jdemo{
	public static void main(String[] args)throws IOException{
		BufferedReader br=new BufferedReader (new InputStreamReader(System.in));
		int rows=Integer.parseInt (br.readLine());
		int col=0;
		int ch=rows+65;
		for(int i=1; i<rows*2; i++){
			if(i<=rows)
				col=rows-i;
			else
				col=i-rows;
			for(int sp=1; sp<=col; sp++){
				System.out.print("\t");
			}
			if(i<=rows)
				col=i;
			else
				col=rows*2-i;
			ch-=col;
					for(int j=1; j<=col; j++){
						System.out.print((char)ch +"\t");
						ch++;
					}
					System.out.println();
		}
	}
}
