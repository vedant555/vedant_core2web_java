import java.io.*;

class Fdemo{
	public static void main(String[] args)throws IOException{
		BufferedReader br=new BufferedReader(new InputStreamReader(System.in));
		int rows=Integer.parseInt(br.readLine());
		for(int i=1; i<=rows; i++){
			for(int sp=rows; sp>i; sp--){
				System.out.print("  ");
			}int num=1;
			for(int j=1; j<=i*2-1; j++){
				System.out.print(num++ +" ");
			}
			System.out.println();
		}
	}
}

