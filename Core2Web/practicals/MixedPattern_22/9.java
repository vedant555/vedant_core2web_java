import java.io.*;

class IDemo{
	public static void main(String[] args)throws IOException{
		BufferedReader br=new BufferedReader(new InputStreamReader(System.in));
		int row=Integer.parseInt(br.readLine());
		int num1=row;
		for(int i=1; i<=row; i++){
			for(int sp=1; sp<=row-i; sp++){
				System.out.print("\t");
			}int num2=num1;
			for(int j=1; j<=i*2-1; j++){
				System.out.print(num2--+"\t");
			}num1++;
			System.out.println();
		}
	}
}
