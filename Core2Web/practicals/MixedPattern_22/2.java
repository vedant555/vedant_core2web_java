import java.io.*;

class Bdemo{
	public static void main(String[] args)throws IOException{
		BufferedReader br=new BufferedReader(new InputStreamReader(System.in));
		int rows=Integer.parseInt(br.readLine());
		int num=1;
		for(int i=1; i<=rows; i++){
			for(int space=1; space<i; space++){
				System.out.print("  ");
			}
			for(int j=rows; j>=i; j--){
				System.out.print(num +" ");
				num++;				
			}num--;
			System.out.println();
		}
	}
}
