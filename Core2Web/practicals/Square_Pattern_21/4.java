import java.io.*;

class Ddemo{
	public static void main(String[] args)throws IOException{
		BufferedReader br=new BufferedReader(new InputStreamReader(System.in));
		int rows=Integer.parseInt(br.readLine());
		int num=rows;
		
		for(int i=1; i<=rows; i++){
			for(int j=1; j<=rows; j++){
				if(j==1){
					int ch=num+64;
					
					System.out.print((char)ch+" ");
				}else{
					
					System.out.print(num+" ");
				}
				num++;
			}
			System.out.println();
		}
	}
}
