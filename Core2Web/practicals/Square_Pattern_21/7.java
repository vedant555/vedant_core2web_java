import java.io.*;

class Gdemo{
	public static void main(String[] args)throws IOException{
		BufferedReader br=new BufferedReader(new InputStreamReader(System.in));
		int rows=Integer.parseInt(br.readLine());
		int num=rows;
		for(int i=1; i<=rows; i++){
			int num2=i+64;
			for(int j=1; j<=rows; j++){
				if(num%2==1){
					System.out.print((char)num2 +" ");
				}else{
					System.out.print(num+" ");
				}num++;
			}
			System.out.println();
		}
	}
}
